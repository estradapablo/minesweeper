import * as AWS from "aws-sdk";
import { v4 as uuidv4 } from 'uuid';
import { QueryCreate, ParamsCreate, Mine } from '../helpers/types'

let options = null;
if (process.env.IS_OFFLINE) {
    options = {
        region: "localhost",
        endpoint: "http://localhost:8000"
    }
}

const queryCreateGames = async (params: ParamsCreate, table: Array<Mine>[]) => {

    let { cols, rows, numberMines, username } = params;

    /** Creamos el query y guardamos la data**/
    let id = uuidv4();
    let query: QueryCreate = {
        TableName: 'games',
        Item: {
            username: username || 'None',
            id: id,
            statusGame: 'in_process',
            gameTurn: 0,
            initNumberMines: numberMines || 25,
            numberMines: numberMines || 25,
            opened: 0,
            cols: cols || 10,
            rows: rows || 0,
            mines: table
        }
    };


    const docClient = new AWS.DynamoDB.DocumentClient(options);

    return new Promise((resolve) => {

        docClient.put(query, function (error) {
            if (error) {
                return resolve(error)
            } else {
                return resolve({ id: id });
            }
        });

    });

}

export default queryCreateGames