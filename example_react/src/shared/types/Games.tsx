export type Games = {
    id?: any,
    statusGame: string,
    gameTurn: number,
    numberMines: number,
    opened: number,
    cols: number,
    rows: number,
    mines: Array<Array <Mine> >
}


export type Mine = {
    col: number,
    row: number,
    mine: boolean,
    open: boolean,
    nearMines: number
    flag: string
}