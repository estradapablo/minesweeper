import * as Joi from '@hapi/joi';

/** Definimos esquema de validacion para acciones y turno siguiente**/
export const schema = Joi.object({
  id: Joi.string()
    .required(),
  row: Joi.number()
    .required(),
  col: Joi.number()
    .required()
})
