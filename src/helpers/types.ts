export type Mine = {
    col: number,
    row: number,
    mine: boolean,
    open: boolean,
    nearMines: number
    flag: string
}

export type Game = {
    id: any,
    username: string,
    statusGame: string,
    gameTurn: number,
    initNumberMines: number,
    numberMines: number,
    opened: number,
    cols: number,
    rows: number,
    mines: Array<Array<Mine>>
}

export type QueryCreate = {
    TableName: string,
    Item: Game
}

export type QueryUpdate = {
    TableName: string,
    Key: Object,
    UpdateExpression: string,
    ExpressionAttributeValues: any
    ReturnValues: string
}

export type QueryGet = {
    TableName: string,
    Key: {
        id: string
    }
};

export type ParamsGet = {
    id: string
};


export type ParamsCreate = {
    cols: number,
    rows: number,
    numberMines: number,
    username: string
};

export type ParamsActionsNextTurn = {
    id: string,
    row: number,
    col: number
};
