import axios from 'axios';

export const CREATE_GAME = 'CREATE_GAME';
export const GET_GAME = 'GET_GAME';
export const NEXT_TURN = 'NEXT_TURN';
export const ACTIONS = 'ACTIONS';

type Action = { type: string, payload: string | object };


export const createGame = (payload: Object): Action => ({
  type: CREATE_GAME,
  payload: axios
    .post('/create', payload)
    .then((response: any) => ({ game: response.data }))
    .catch((error: any) => Promise.reject(error))
});

export const getGame = (id: string) => ({
  type: GET_GAME,
  payload: axios
    .post('/get-game', id)
    .then((response: any) => ({ game: response.data }))
    .catch((error: any) => Promise.reject(error))
});


export const nextTurn = (payload: object) => ({
  type: NEXT_TURN,
  payload: axios
    .post('/next-turn', payload)
    .then((response: any) => ({ game: response.data }))
    .catch((error: any) => Promise.reject(error))
});

export const actions = (payload: object) => ({
  type: ACTIONS,
  payload: axios
    .post('/action', payload)
    .then((response: any) => ({ game: response.data }))
    .catch((error: any) => Promise.reject(error))
});
