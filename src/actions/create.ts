import { schema } from '../models/createGame'
import { Mine, ParamsCreate } from '../helpers/types'
import queryCreateGames from '../querys/queryCreateGame'

/**
 * @api {post} /$env/create Create a New Game
 * @apiVersion 1.0.0
 * @apiName CreateGame
 * @apiGroup Login
 *
 * @apiParam {String} username Name to start game.
 * @apiParam {Number} [rows] Number of Rows to start game.
 * @apiParam {Number} [cols] Number of Cols to start game.
 * @apiParam {Number} [numberMines] Number of mines to start game.
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *      "cols": 10,
 *      "rows": 10,
 *      "numberMines": 25
 *     }
 *
 * @apiSuccess {String} id If of new game.
 * 
 *  * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "id": "e0f36229-8575-4d7a-babb-cd5d7f453d36"
 *     }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 442 Unprocessable Entity
 *     {
 *        "_original": {
 *             "cols": 10,
 *            "rows": 10,
 *             "numberMines": 25
 *         },
 *        "details": [
 *            {
 *                "message": "\"username\" is required",
 *                "path": [
 *                    "username"
 *                ],
 *                "type": "any.required",
 *                "context": {
 *                    "label": "username",
 *                    "key": "username"
 *                }
 *            }
 *        ],
 *        "statusCode": 422
 *    }
 */

const CreateGame = async (params: ParamsCreate) => {

  try {

    /** Validamos los parametros segun el esquema definido en Models**/
    await schema.validateAsync(params);

    let { cols, rows } = params;

    /** Default cols, rows si vienen vacias.**/
    if (!rows) { rows = 10 }
    if (!cols) { cols = 10 }

    let table = [];
    for (let numberOfRow = 0; numberOfRow < rows; numberOfRow++) {

      table.push([]);

      for (let numberOfCol = 0; numberOfCol < cols; numberOfCol++) {

        table[numberOfRow].push(<Mine>{
          col: numberOfCol,
          row: numberOfRow,
          mine: false,
          open: false,
          nearMines: 0,
          flag: 'none'
        });

      }

    }

    return queryCreateGames(params, table)

  }
  catch (err) {

    /** Si no paso la validacion del request, devolvemos error **/
    err.statusCode = 422;
    return err
  }

}

export default CreateGame