import React from 'react';
import { Games, Mine } from '../../shared/types/Games';

import { Container, Row, Col, Table } from 'reactstrap';
import { Icon } from 'react-icons-kit'
import { bomb } from 'react-icons-kit/fa/bomb'
import { flag as flagIcon } from 'react-icons-kit/fa/flag'
import { question } from 'react-icons-kit/fa/question'

import FinalBoardContainer from './boards/finalBoard/FinalBoardContainer';

import LoadingContainer from '../shared/LoadingContainer'

import '../../shared/styles/GameBoard.css';

type Props = {
    loading: boolean,
    error: boolean,
    game?: Games,
    handleNextTurn: Function,
    handleActions: Function
};

function GameBoardPresentational(props: Props) {

    const { loading, error, game, handleNextTurn, handleActions } = props;

    return (
        <Container className="d-flex h-100 justify-content-center">
            <Row className="h-100 align-self-cente">
                <Col className="board">
                    {loading ? <LoadingContainer /> : null}
                    {error ? <p>Sorry, we have an error...</p> : null}
                    {!loading && game && !error ?

                        < React.Fragment >

                            <Row className={(game.statusGame === '' ? 'd-none' : '') + " mt-5 table-class ml-0 mr-0"}>
                                <Col className="text-center red black-background font-weight-bold"> {game.numberMines}</Col>
                                <Col className="black-grey-dark text-center"></Col>
                                <Col className="text-center red black-background font-weight-bold"> 000</Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Table borderless className="table-class">
                                        <tbody>
                                            {
                                                game.mines.map((row, indexRow) => {
                                                    return <tr key={indexRow} >
                                                        {
                                                            row.map((mines: Mine, indexCols) => {
                                                                const { open, mine, nearMines, flag, row, col } = mines;
                                                                let nearMineClass = ''
                                                                switch (nearMines) {
                                                                    case 1:
                                                                        nearMineClass = 'blue'
                                                                        break;
                                                                    case 2:
                                                                        nearMineClass = 'green'
                                                                        break;
                                                                    case 3:
                                                                        nearMineClass = 'red'
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }

                                                                const { id, statusGame } = game

                                                                return <td key={indexCols} className="m-0 p-0">

                                                                    <div onClick={() => handleNextTurn(row, col, id, open)}
                                                                        onContextMenu={(contextMenu) => handleActions(contextMenu, row, col, id, open)}
                                                                        className={(open ? 'openCell' : (statusGame !== 'in_process' ? 'closeCell' : 'closeCell pointer')) + ' text-center font-weight-bold ' + nearMineClass}
                                                                    >
                                                                        {statusGame !== 'in_process' && mine ? <Icon icon={bomb} size={10} /> : ''}
                                                                        {nearMines > 0 && open ? nearMines : ''}
                                                                        {statusGame === 'in_process' && flag === 'flag' ? <Icon icon={flagIcon} className="red" size={10} /> : ''}
                                                                        {statusGame === 'in_process' && flag === 'question' ? <Icon icon={question} className="black" size={10} /> : ''}
                                                                    </div>
                                                                </td>

                                                            })
                                                        }
                                                    </tr>

                                                })

                                            }
                                        </tbody>
                                    </Table>

                                </Col>
                            </Row>

                            <FinalBoardContainer game={game} />


                        </React.Fragment>
                        : null}
                </Col>
            </Row>
        </Container >
    );
}

export default GameBoardPresentational