define({ "api": [
  {
    "type": "post",
    "url": "/$env/action",
    "title": "Action Event",
    "version": "1.0.0",
    "name": "Action",
    "group": "Game",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id of the game.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "row",
            "description": "<p>Cliked position of Row.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "col",
            "description": "<p>Cliked position of Col.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\"id\": \"027936e9-1c24-4875-bbc3-5f12e3d73a32\",\n\"row\": 8,\n\"col\": 7\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "mines",
            "description": "<p>Mines field.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "mines.mine",
            "description": "<p>If it's a Mine</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.col",
            "description": "<p>Col Position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.nearMines",
            "description": "<p>Near Mines from this position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.row",
            "description": "<p>Row position</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mines.flag",
            "description": "<p>If it's a flag true ('none', 'flag', 'question')</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "mines.open",
            "description": "<p>If it's Open</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "gameTurn",
            "description": "<p>Current Turn of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "statusGame",
            "description": "<p>Current status of the game ('in_process', 'finished', 'game_over').</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "opened",
            "description": "<p>Numer of fields opened.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rows",
            "description": "<p>Number of rows in the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "initNumberMines",
            "description": "<p>Initial number of mines in the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "numberMines",
            "description": "<p>Current number of mines flaged.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n{\n    \"mines\": [\n        [\n            {\n                \"mine\": false,\n                \"col\": 0,\n                \"nearMines\": 0,\n                \"row\": 0,\n                \"flag\": \"flag\",\n                \"open\": false\n            }\n        ]\n    ],\n    \"gameTurn\": 1,\n    \"statusGame\": \"in_process\",\n    \"opened\": 1,\n    \"id\": \"9bbeb618-0677-4f29-a4d4-1c5f6aa77ff3\",\n    \"rows\": 1,\n    \"cols\": 1,\n    \"initNumberMines\": 25,\n    \"username\": \"Pablo\",\n    \"numberMines\": 25\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 442 Unprocessable Entity\n{\n   \"statusCode\": 422,\n   \"message\": {\n       \"message\": \"No Game with this id: 10129f68-768d-4b96-8810-fe6e6825c3fe\"\n   }\n}",
          "type": "json"
        },
        {
          "title": "Error-Validation-Response:",
          "content": "  HTTP/1.1 442 Unprocessable Entity\n{\n    \"_original\": {},\n    \"details\": [\n        {\n            \"message\": \"\\\"id\\\" is required\",\n            \"path\": [\n                \"id\"\n            ],\n            \"type\": \"any.required\",\n            \"context\": {\n                \"label\": \"id\",\n                \"key\": \"id\"\n            }\n        }\n    ],\n    \"statusCode\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/actions/action.ts",
    "groupTitle": "Game"
  },
  {
    "type": "post",
    "url": "/$env/get-game",
    "title": "Get a Game",
    "version": "1.0.0",
    "name": "GetName",
    "group": "Game",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id to get game.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n \"id\": \"10129f68-768d-4b96-8810-fe6e6825c3fe\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "mines",
            "description": "<p>Mines field.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "mines.mine",
            "description": "<p>If it's a Mine</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.col",
            "description": "<p>Col Position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.nearMines",
            "description": "<p>Near Mines from this position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.row",
            "description": "<p>Row position</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mines.flag",
            "description": "<p>If it's a flag true ('none', 'flag', 'question')</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "mines.open",
            "description": "<p>If it's Open</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "gameTurn",
            "description": "<p>Current Turn of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "statusGame",
            "description": "<p>Current status of the game ('in_process', 'finished', 'game_over').</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "opened",
            "description": "<p>Numer of fields opened.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rows",
            "description": "<p>Number of rows in the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "initNumberMines",
            "description": "<p>Initial number of mines in the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "numberMines",
            "description": "<p>Current number of mines flaged.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n{\n    \"mines\": [\n        [\n            {\n                \"mine\": false,\n                \"col\": 0,\n                \"nearMines\": 0,\n                \"row\": 0,\n                \"flag\": \"none\",\n                \"open\": false\n            }\n        ]\n    ],\n    \"gameTurn\": 0,\n    \"statusGame\": \"in_process\",\n    \"opened\": 0,\n    \"id\": \"9bbeb618-0677-4f29-a4d4-1c5f6aa77ff3\",\n    \"rows\": 1,\n    \"cols\": 1,\n    \"initNumberMines\": 25,\n    \"username\": \"Pablo\",\n    \"numberMines\": 25\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 442 Unprocessable Entity\n{\n   \"statusCode\": 422,\n   \"message\": {\n       \"message\": \"No Game with this id: 10129f68-768d-4b96-8810-fe6e6825c3fe\"\n   }\n}",
          "type": "json"
        },
        {
          "title": "Error-Validation-Response:",
          "content": "  HTTP/1.1 442 Unprocessable Entity\n{\n    \"_original\": {},\n    \"details\": [\n        {\n            \"message\": \"\\\"id\\\" is required\",\n            \"path\": [\n                \"id\"\n            ],\n            \"type\": \"any.required\",\n            \"context\": {\n                \"label\": \"id\",\n                \"key\": \"id\"\n            }\n        }\n    ],\n    \"statusCode\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/actions/getGame.ts",
    "groupTitle": "Game"
  },
  {
    "type": "post",
    "url": "/$env/next-turn",
    "title": "Pass Next Turn",
    "version": "1.0.0",
    "name": "NextTurn",
    "group": "Game",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id of the game.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "row",
            "description": "<p>Cliked position of Row.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "col",
            "description": "<p>Cliked position of Col.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n\"id\": \"027936e9-1c24-4875-bbc3-5f12e3d73a32\",\n\"row\": 8,\n\"col\": 7\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "mines",
            "description": "<p>Mines field.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "mines.mine",
            "description": "<p>If it's a Mine</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.col",
            "description": "<p>Col Position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.nearMines",
            "description": "<p>Near Mines from this position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "mines.row",
            "description": "<p>Row position</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mines.flag",
            "description": "<p>If it's a flag true ('none', 'flag', 'question')</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "mines.open",
            "description": "<p>If it's Open</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "gameTurn",
            "description": "<p>Current Turn of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "statusGame",
            "description": "<p>Current status of the game ('in_process', 'finished', 'game_over').</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "opened",
            "description": "<p>Numer of fields opened.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "rows",
            "description": "<p>Number of rows in the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "initNumberMines",
            "description": "<p>Initial number of mines in the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the game.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "numberMines",
            "description": "<p>Current number of mines flaged.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n{\n    \"mines\": [\n        [\n            {\n                \"mine\": true,\n                \"col\": 0,\n                \"nearMines\": 0,\n                \"row\": 0,\n                \"flag\": \"none\",\n                \"open\": false\n            }\n        ]\n    ],\n    \"gameTurn\": 1,\n    \"statusGame\": \"in_process\",\n    \"opened\": 1,\n    \"id\": \"9bbeb618-0677-4f29-a4d4-1c5f6aa77ff3\",\n    \"rows\": 1,\n    \"cols\": 1,\n    \"initNumberMines\": 25,\n    \"username\": \"Pablo\",\n    \"numberMines\": 25\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "    HTTP/1.1 442 Unprocessable Entity\n{\n   \"statusCode\": 422,\n   \"message\": {\n       \"message\": \"No Game with this id: 10129f68-768d-4b96-8810-fe6e6825c3fe\"\n   }\n}",
          "type": "json"
        },
        {
          "title": "Error-Validation-Response:",
          "content": "  HTTP/1.1 442 Unprocessable Entity\n{\n    \"_original\": {},\n    \"details\": [\n        {\n            \"message\": \"\\\"id\\\" is required\",\n            \"path\": [\n                \"id\"\n            ],\n            \"type\": \"any.required\",\n            \"context\": {\n                \"label\": \"id\",\n                \"key\": \"id\"\n            }\n        }\n    ],\n    \"statusCode\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/actions/nextTurn.ts",
    "groupTitle": "Game"
  },
  {
    "type": "post",
    "url": "/$env/create",
    "title": "Create a New Game",
    "version": "1.0.0",
    "name": "CreateGame",
    "group": "Login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Name to start game.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "rows",
            "description": "<p>Number of Rows to start game.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "cols",
            "description": "<p>Number of Cols to start game.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "numberMines",
            "description": "<p>Number of mines to start game.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n \"cols\": 10,\n \"rows\": 10,\n \"numberMines\": 25\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>If of new game.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": \"e0f36229-8575-4d7a-babb-cd5d7f453d36\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 442 Unprocessable Entity\n {\n    \"_original\": {\n         \"cols\": 10,\n        \"rows\": 10,\n         \"numberMines\": 25\n     },\n    \"details\": [\n        {\n            \"message\": \"\\\"username\\\" is required\",\n            \"path\": [\n                \"username\"\n            ],\n            \"type\": \"any.required\",\n            \"context\": {\n                \"label\": \"username\",\n                \"key\": \"username\"\n            }\n        }\n    ],\n    \"statusCode\": 422\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/actions/create.ts",
    "groupTitle": "Login"
  }
] });
