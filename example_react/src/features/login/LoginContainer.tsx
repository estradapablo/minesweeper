import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeName, changeRows, changeCols, changeMines } from '../../model/actions/login';
import { createGame } from '../../model/actions/game';

import { useHistory } from "react-router-dom";

import LoginPresentational from './LoginPresentational';

type Props = {
  handleOnChangeName: Function,
  handleOnChangeRows: Function,
  handleOnChangeCols: Function,
  handleOnChangeMines: Function,
  handleOnCreate: Function,
  username: string,
  rows: string,
  cols: string,
  numberMines: string,
  game: object
};

function LoginContainer(props: Props) {

  const { handleOnChangeName, 
          handleOnChangeRows, 
          handleOnChangeCols, 
          handleOnChangeMines, 
          handleOnCreate, 
          username,
          rows,
          cols,
          numberMines
  } = props;

  const history = useHistory();

  const handleCreate = () => {

    handleOnCreate({
      username: username,
      rows: rows,
      cols: cols,
      numberMines: numberMines
    }).then((response:any)=>{

      if(response.value.game.id){
        history.push("/game-board/" + response.value.game.id)
      }else{
        alert('Error to create a new game!')
      }
      
    })
  
  }
  
  return (
    <LoginPresentational 
       handleOnChangeName={handleOnChangeName} 
       handleOnChangeRows={handleOnChangeRows}  
       handleOnChangeCols={handleOnChangeCols}  
       handleOnChangeMines={handleOnChangeMines} 
       handleCreate={handleCreate} username={username} rows={rows} cols={cols} numberMines={numberMines} />
  );
  
}



const mapStateToProps = (state: any) => ({
  username: state.login.username,
  rows: state.login.rows,
  cols: state.login.cols,
  numberMines: state.login.numberMines
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  handleOnChangeName: changeName,
  handleOnChangeRows: changeRows,
  handleOnChangeCols: changeCols,
  handleOnChangeMines: changeMines,
  handleOnCreate: createGame
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)


