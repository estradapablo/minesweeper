import { APIGatewayProxyHandler } from 'aws-lambda';
import 'source-map-support/register';

import HandlerResponse from './helpers/handlerResponse';
import CreateGame from './actions/create';
import GetGame from './actions/getGame';
import NextTurn from './actions/nextTurn';
import Action from './actions/action';

export const handlerRequests: APIGatewayProxyHandler = async (event, _context) => {

  /** 
   * Manejamos las distintas rutas dependiendo el nombre para centralizar todos los request
   * y poder aplicar logicas globales.
  **/

  let params = null
  if (event.body !== null && event.body !== undefined) {
    params = JSON.parse(event.body)
  }

  switch (event.path) {
    case '/create':
      let createGame = await CreateGame(params);
      return HandlerResponse(createGame)

    case '/get-game':
      let getGame = await GetGame(params);
      return HandlerResponse(getGame)

    case '/next-turn':
      let nextTurn = await NextTurn(params);
      return HandlerResponse(nextTurn)

    case '/action':
      let action = await Action(params);
      return HandlerResponse(action)

  }



}
