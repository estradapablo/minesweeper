import * as AWS from "aws-sdk";
let options = null;
if (process.env.IS_OFFLINE) {
    options = {
        region: "localhost",
        endpoint: "http://localhost:8000"
    }
}

import { QueryGet, ParamsGet, Game } from '../helpers/types'

const queryGetGames = async (params: ParamsGet) => {

    let { id } = params;
    /** Creamos el query y devolvemos la data**/
    var query: QueryGet = {
        TableName: 'games',
        Key: {
            id: id
        }
    };

    const docClient = new AWS.DynamoDB.DocumentClient(options);
    
    return new Promise<Game>((resolve) => {

        docClient.get(query, function (error, data) {

            if (error) {
                error.statusCode= 400;
                return resolve(error)
            } else {
                if(!data.Item){
                    let error: any = {statusCode: 422, message: 'No Game with this id: '+ id}
                    return resolve(error)
                }
                return resolve(data.Item);
            }
        });
    });

}

export default queryGetGames