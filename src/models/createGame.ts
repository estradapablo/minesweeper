import * as Joi from '@hapi/joi';

/** Definimos esquema de validacion para crear el juego**/
export const schema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  rows: Joi.number(),
  cols: Joi.number(),
  numberMines: Joi.number()
})
