# Minesweeper Game

Welcome to Minesweeper Game


### Installing

```
yarn
```

or

```
npm install
```

## Running the tests

yarn test

### End to end tests

Screenshots, reducers and actions test

```
 PASS  src/features/login/Login.test.tsx
 PASS  src/features/gameBoard/boards/finalBoard/FinalBoard.test.tsx
 PASS  src/features/gameBoard/Game.test.tsx

Test Suites: 3 passed, 3 total
Tests:       14 passed, 14 total
Snapshots:   3 passed, 3 total
Time:        2.399s

```

### And coding style tests
```
yarn storybook
```

## Build FIle
```
yarn build
```

## Deploy
For deploy in S3 Bucket

```
yarn deploy
```

## Built With

* [React](https://es.reactjs.org/docs/) - The web framework used

## Authors

* **Pablo Estrada** - *Initial work* - [Pablo Estrada](https://bitbucket.org/estradapablo/)

