import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer';

import { Provider } from 'react-redux';
import { store } from '../../model/Store';

import LoginContainer from './LoginContainer';
import LoginPresentational from './LoginPresentational';
import { changeName, changeRows, CHANGE_NAME, CHANGE_ROWS, changeCols, CHANGE_COLS, CHANGE_MINES, changeMines } from '../../model/actions/login';
import loginForm from '../../model/reducers/login';
import { initialState } from '../../model/reducers/login';

let mocFun = jest.fn();

describe('LoginContainer', () => {

  it('LoginContainer Render', () => {
    const tree = renderer
      .create(<MemoryRouter>
        <Provider store={store}>
            <LoginContainer />
        </Provider>
      </MemoryRouter>)
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Exist LoginPresentational ', () => {

    const wrapper = shallow(<Provider store={store}><LoginContainer /></Provider>);

    expect(wrapper.getElements(<LoginPresentational handleOnChangeName={mocFun} />).length).toEqual(1);

  });


});

describe('LoginPresentational', () => {

  it('Exist Input "login-input-name" && Button "login-button" ', () => {

    const wrapper = shallow(<LoginPresentational handleOnChangeName={mocFun} />);

    expect(wrapper.find('#login-input-name').length).toEqual(1);

  });

});

describe('Login Actions', () => {

  it('should create an action to change name', () => {
    const payload = 'NAME'
    const expectedAction = {
      type: CHANGE_NAME,
      payload
    }
    expect(changeName(payload)).toEqual(expectedAction)
  })

  it('should create an action to change rows', () => {
    const payload = "25"
    const expectedAction = {
      type: CHANGE_ROWS,
      payload
    }
    expect(changeRows(payload)).toEqual(expectedAction)
  })

  it('should create an action to change cols', () => {
    const payload = "25"
    const expectedAction = {
      type: CHANGE_COLS,
      payload
    }
    expect(changeCols(payload)).toEqual(expectedAction)
  })

  it('should create an action to change mines', () => {
    const payload = "25"
    const expectedAction = {
      type: CHANGE_MINES,
      payload
    }
    expect(changeMines(payload)).toEqual(expectedAction)
  })

})


describe('Login Reducer', () => {

  it('should return the initial state', () => {
    expect(loginForm(initialState, { type: CHANGE_NAME, payload: '' })).toEqual({
      username: '',
      cols: '10',
      rows: '10',
      numberMines: '25'
    })
  })

  it('should handle CHANGE_NAME', () => {
    expect(
      loginForm(initialState, { type: CHANGE_NAME, payload: 'Nombre' })
    ).toEqual({
      username: 'Nombre',
      cols: "10",
      numberMines: "25",
      rows: "10"
    })

  })

  it('should handle CHANGE_ROWS', () => {
    expect(
      loginForm(initialState, { type: CHANGE_ROWS, payload: '25' })
    ).toEqual({
      username: '',
      cols: "10",
      numberMines: "25",
      rows: "25"
    })

  })

  it('should handle CHANGE_COLS', () => {
    expect(
      loginForm(initialState, { type: CHANGE_COLS, payload: '25' })
    ).toEqual({
      username: '',
      cols: "25",
      numberMines: "25",
      rows: "10"
    })
  })

  it('should handle CHANGE_MINES', () => {
    expect(
      loginForm(initialState, { type: CHANGE_MINES, payload: '20' })
    ).toEqual({
      username: '',
      cols: "10",
      numberMines: "20",
      rows: "10"
    })
  })

})

