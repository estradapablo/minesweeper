import axios from 'axios';
import {config} from '../config/config';

const { API_ENV, API_URL } = config;

export const initializeAxios = () => {
  const api_url = `${API_URL}${API_ENV}`;
  axios.defaults.baseURL = api_url;
  axios.defaults.headers.common = {
    ...axios.defaults.headers.common,
    'Content-Type': 'application/json',
    'Accept': 'application/json'
 };
};

