import { CHANGE_NAME, CHANGE_ROWS, CHANGE_COLS, CHANGE_MINES } from '../actions/login';

type Action = { type: string, payload: string };

type State = {
  username: string,
  cols: string,
  rows: string,
  numberMines: string,
  isFetching?: boolean,
  didInvalidate?: boolean
};

export const initialState: State = {
  username: '',
  cols: '10',
  rows: '10',
  numberMines: '25'
};

const loginForm = (state: State = initialState, action: Action): State => {
  switch (action.type) {

    case `${CHANGE_NAME}`:
      return {
        ...state,
        username: action.payload,
      };
    case `${CHANGE_ROWS}`:
      return {
        ...state,
        rows: action.payload,
      };
    case `${CHANGE_COLS}`:
      return {
        ...state,
        cols: action.payload,
      };
    case `${CHANGE_MINES}`:
      return {
        ...state,
        numberMines: action.payload,
      };
    default:
      return state;
  }
};

export default loginForm;

