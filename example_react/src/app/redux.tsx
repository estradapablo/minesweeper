import React from 'react';
import { Provider } from 'react-redux';
import { store, persistor } from '../model/Store';
import { PersistGate } from 'redux-persist/integration/react'

const ReduxHOC = (App: any) => class ReduxComponent extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <App {...this.props} />
        </PersistGate>
      </Provider>
    );
  }
};

export default ReduxHOC;