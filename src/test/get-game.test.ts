import * as AWSMock from "aws-sdk-mock";
import * as AWS from "aws-sdk";
import { Get } from "aws-sdk/clients/dynamodb";
import { expect } from 'chai';

const getQueryResponse = require("./mockData/get_query_response.json")
const getResponse = require("./mockData/get_response.json")


import { handlerRequests } from '../index'
const LambdaTester = require('lambda-tester');

describe("Minesweeper API TEST /$env/get-game", () => {

    AWSMock.setSDKInstance(AWS);
    AWSMock.mock('DynamoDB.DocumentClient', 'get', (query: Get, callback: Function) => {
        callback(null, getQueryResponse);
    })

    it("Get Game in DynamoDB - Successfully", async () => {

        const params: any = {
            "id": "bcf10e78-111a-4909-b018-2e091e0100e2"
        };

        await LambdaTester(handlerRequests)
            .event({
                httpMethod: 'POST',
                path: '/get-game',
                body: JSON.stringify(params)
            })
            .expectResult((result) => {

               result.body = JSON.parse(result.body) 

               expect(result).to.have.property('statusCode');
               expect(result.statusCode).to.equal(200);
               expect(result.body.id).to.equal(getResponse.id );
               expect(result.body.mines.length).to.equal(10);
         
            });

    });

    it("Get Game in DynamoDB - Error", async () => {

        const params: any = {
        };

        await LambdaTester(handlerRequests)
            .event({
                httpMethod: 'POST',
                path: '/get-game',
                body: JSON.stringify(params)
            })
            .expectResult((result) => {

               result.body = JSON.parse(result.body) 

               expect(result).to.have.property('statusCode');
               expect(result.statusCode).to.equal(422);

            });

    });


});