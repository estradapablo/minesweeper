import React from 'react';

type Props = {
	text?: string
};

function LoadingContainer(props: Props) {
    const { text } = props;
    return (
        <div className="text-center loading-paddig">
            <div className="align-self-center"><img src={'/ring.gif'} width="55" alt={text || 'loading...'}></img></div>
            <div className="align-self-center ml-3">{text || 'loading...'}</div>
        </div>
    );
}

export default LoadingContainer