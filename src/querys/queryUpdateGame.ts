import * as AWS from "aws-sdk";
let options = null;
if (process.env.IS_OFFLINE) {
    options = {
        region: "localhost",
        endpoint: "http://localhost:8000"
    }
}

import { QueryUpdate, Game } from '../helpers/types'

const queryUpdateGames = async (game: Game) => {

    /** Creamos el query y guardamos la data**/
    var query: QueryUpdate = {
        TableName: 'games',
        Key: {
            id: game.id
        },
        UpdateExpression: "set mines = :m, gameTurn=:g, opened=:o, statusGame=:s, numberMines=:n",
        ExpressionAttributeValues:{
            ":m": game.mines,
            ":g": game.gameTurn,
            ":o": game.opened,
            ":s": game.statusGame,
            ":n": game.numberMines
        },
        ReturnValues: 'ALL_NEW'
    };

    const docClient = new AWS.DynamoDB.DocumentClient(options);

    return new Promise((resolve) => {

        docClient.update(query, function (error, data) {
            if (error) {
                error.statusCode= 400;
                return resolve(error)
            } else {
                return resolve(data.Attributes);
            }
        });

    });

}

export default queryUpdateGames