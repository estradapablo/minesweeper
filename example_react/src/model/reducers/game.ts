import { GET_GAME, CREATE_GAME, NEXT_TURN, ACTIONS } from '../actions/game';
import { Games } from '../../shared/types/Games';

type State = {
  game?: Games,
  isFetching: boolean,
  isFetchingTurn: boolean,
  didInvalidate: boolean
};

export const initialState: State = {
  game: {
    id: '',
    statusGame: '',
    gameTurn: 0,
    numberMines: 0,
    opened: 0,
    cols: 0,
    rows: 0,
    mines: []
  },
  isFetching: true,
  isFetchingTurn: true,
  didInvalidate: false
};

const Game = (state: State = initialState, action: any): State => {
  switch (action.type) {
    case `${CREATE_GAME}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case `${CREATE_GAME}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false,
        game: {
          id: '',
          statusGame: '',
          gameTurn: 0,
          numberMines: 0,
          opened: 0,
          cols: 0,
          rows: 0,
          mines: []
        }
      };
    case `${CREATE_GAME}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false
      };
    case `${GET_GAME}_REJECTED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true
      };
    case `${GET_GAME}_PENDING`:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false
      };
    case `${GET_GAME}_FULFILLED`:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        game: action.payload.game
      };

    case `${NEXT_TURN}_REJECTED`:
      return {
        ...state,
        isFetchingTurn: false,
        didInvalidate: true
      };
    case `${NEXT_TURN}_PENDING`:
      return {
        ...state,
        isFetchingTurn: true,
        didInvalidate: false
      };
    case `${NEXT_TURN}_FULFILLED`:
      return {
        ...state,
        isFetchingTurn: false,
        didInvalidate: false,
        game: action.payload.game
      };

    case `${ACTIONS}_REJECTED`:
      return {
        ...state,
        isFetchingTurn: false,
        didInvalidate: true
      };
    case `${ACTIONS}_PENDING`:
      return {
        ...state,
        isFetchingTurn: true,
        didInvalidate: false
      };
    case `${ACTIONS}_FULFILLED`:
      return {
        ...state,
        isFetchingTurn: false,
        didInvalidate: false,
        game: action.payload.game
      };
    default:
      return state;
  }
};

export default Game;

