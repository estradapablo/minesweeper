const HandlerResponse = (body) => {

    const headers = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Content-Type",
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
    }

    /** Si tiene codigo de error devolvemos con error y codigo**/
    if(body.statusCode){
        return {
            headers,
            statusCode: body.statusCode,
            body: JSON.stringify(body, null, 2)
        };
    }

    /** Si no viene codigo de error, devolvemos 200**/
    return {
        headers,
        statusCode: 200,
        body: JSON.stringify(body, null, 2),
    };

}

export default HandlerResponse