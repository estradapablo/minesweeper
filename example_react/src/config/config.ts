export const config = {
    API_URL: process.env.REACT_APP_API_URL || 'http://localhost:3000/',
    API_ENV: (process.env.NODE_ENV === 'development' ? 'dev' : 'dev'),
};

