import * as Joi from '@hapi/joi';

/** Definimos esquema de validacion para buscar una partida**/
export const schema = Joi.object({
    id: Joi.string()
        .required()
})