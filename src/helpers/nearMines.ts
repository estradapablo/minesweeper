import { Game } from './types'

export default function openCells(game: Game) {

     /** Recorremos el Array (Eje Y ) Y < COLS de arriba hacia abajo**/
    for (let x = 0; x < game.rows; x++) {
         /*** Recorremos la segunda matriz (Eje X) X < COLS de derecha a izquierda***/
        for (let y = 0; y < game.cols; y++) {

            /** Si es una mina, continuamos con el proximo**/
            if (game.mines[x][y].mine)
                continue;

             /** Definimos cantidad de minas cercanas**/       
            let nearMines = 0;

            /** Recorremos las minas al sobre el eje X desde la posicion actual -1 +1**/
            for (let i = -1; i <= 1; i++) {

                /** Recorremos las minas al sobre el eje y desde la posicion actual -1 +1**/
                for (let j = -1; j <= 1; j++) {
                    try {
                        let item = game.mines[(x + i)][(y + j)];
                         /** si existe y es una mina sumamos al contador**/
                        if (item != null && item.mine && item != undefined)
                            nearMines++;
                    } catch (error) {
                        /** si no es una mina, continuamos**/
                        continue;
                    }
                }
            }
            game.mines[x][y].nearMines = nearMines;
        }
    }

}