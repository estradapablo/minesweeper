import { ParamsActionsNextTurn } from '../helpers/types';
import { schema } from '../models/actionsNextTurn'

import queryGetGames from '../querys/queryGetGames'
import queryUpdateGames from '../querys/queryUpdateGame'

/**
 * @api {post} /$env/action Action Event
 * @apiVersion 1.0.0
 * @apiName Action
 * @apiGroup Game
 *
 * @apiParam {String} id id of the game.
 * @apiParam {Number} row Cliked position of Row.
 * @apiParam {Number} col Cliked position of Col.
 *
 * @apiParamExample {json} Request-Example:
 *   {
 *   "id": "027936e9-1c24-4875-bbc3-5f12e3d73a32",
 *   "row": 8,
 *   "col": 7
 *   }
 *
 * @apiSuccess {Object[]} mines Mines field.
 * @apiSuccess {Boolean} mines.mine If it's a Mine
 * @apiSuccess {Number} mines.col Col Position
 * @apiSuccess {Number} mines.nearMines Near Mines from this position
 * @apiSuccess {Number} mines.row Row position
 * @apiSuccess {String} mines.flag If it's a flag true ('none', 'flag', 'question')
 * @apiSuccess {Boolean} mines.open If it's Open
 * 
 * @apiSuccess {Number} gameTurn Current Turn of the game.
 * @apiSuccess {String} statusGame Current status of the game ('in_process', 'finished', 'game_over').
 * @apiSuccess {Number} opened Numer of fields opened.
 * @apiSuccess {String} id id of the game.
 * @apiSuccess {Number} rows Number of rows in the game.
 * @apiSuccess {Number} initNumberMines Initial number of mines in the game.
 * @apiSuccess {String} username Username of the game.
 * @apiSuccess {Number} numberMines Current number of mines flaged.
 * 
 *  * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *  {
 *      "mines": [
 *          [
 *              {
 *                  "mine": false,
 *                  "col": 0,
 *                  "nearMines": 0,
 *                  "row": 0,
 *                  "flag": "flag",
 *                  "open": false
 *              }
 *          ]
 *      ],
 *      "gameTurn": 1,
 *      "statusGame": "in_process",
 *      "opened": 1,
 *      "id": "9bbeb618-0677-4f29-a4d4-1c5f6aa77ff3",
 *      "rows": 1,
 *      "cols": 1,
 *      "initNumberMines": 25,
 *      "username": "Pablo",
 *      "numberMines": 25
 *  }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 442 Unprocessable Entity
 * {
 *    "statusCode": 422,
 *    "message": {
 *        "message": "No Game with this id: 10129f68-768d-4b96-8810-fe6e6825c3fe"
 *    }
 * }
 * 
 * @apiErrorExample {json} Error-Validation-Response:
 *     HTTP/1.1 442 Unprocessable Entity
 *   {
 *       "_original": {},
 *       "details": [
 *           {
 *               "message": "\"id\" is required",
 *               "path": [
 *                   "id"
 *               ],
 *               "type": "any.required",
 *               "context": {
 *                   "label": "id",
 *                   "key": "id"
 *               }
 *           }
 *       ],
 *       "statusCode": 422
 *   }
 */

const Action = async (params: ParamsActionsNextTurn) => {


  try {

    /** Validamos los parametros segun el esquema definido en Models**/
    await schema.validateAsync(params);
    
    let { id, row, col } = params;

    /** Funcion para buscar el juego en la base **/
    let game = await queryGetGames({ id })
    let { mines } = game;

    /** Si no existe item en esta posicion devolvemos error**/
    if(!mines[row][col]){
      let err = {
        statusCode: 422,
        message: 'No Mine in this combination row:' + row + ' col:'+ col
      };
      return err
    }

    /** Swich con logica de banderas**/
    switch (mines[row][col].flag) {
      case 'none':
        mines[row][col].flag = 'flag';
        game.numberMines = game.numberMines - 1;
        break;
      case 'flag':
        mines[row][col].flag = 'question';
        game.numberMines = game.numberMines + 1;
        break;
      case 'question':
        mines[row][col].flag = 'none';
        break;
      default:
        
        break;
    }

    /** Guardamos el juego antes de mandar la respuesta **/
    return queryUpdateGames(game);

  }
  catch (err) {
    /** Si no paso la validacion del request, devolvemos error **/
    err.statusCode= 422
    return err

  }

}

export default Action