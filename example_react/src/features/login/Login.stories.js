import React from 'react';
import { MemoryRouter } from 'react-router-dom';

import { Provider } from 'react-redux';
import { store } from '../../model/Store';

import LoginPresentational from './LoginPresentational';

export default {
    title: 'Login',
    component: LoginPresentational,
};

export const GameBoard = () => <MemoryRouter><Provider store={store}>
    <LoginPresentational />
</Provider></MemoryRouter>;

GameBoard.story = {
    name: 'Login',
};