import { Mine } from './types'

export default function setMines(numberMines: number, mines: Mine [][], rows: number, cols: number, selectedRow: number, selectedCol: number) {
    for (let mine = 0; mine < numberMines; mine++) {
        let cell = mines[Math.floor(Math.random() * rows)][Math.floor(Math.random() * cols)];
        if (cell.mine) {
            /** Si ya tiene mina, no hacemos nada y le descontamos 1 a minas para que siga una vuelta mas**/
            mine--;
        }
        else if (selectedCol == cell.col && selectedRow == cell.row) {
            /** Si es la posicion donde hizo el primer clik, omitimos y descontamos a mina para que siga**/
            mine--;
        }
        else {
            /** Agregamos la mina donde no se cumples las otras dos condiciones **/
            cell.mine = true;
        }
    }
}