import React from 'react';
import renderer from 'react-test-renderer';

import { Provider } from 'react-redux';
import { store } from '../../../../model/Store';

import FinalBoardContainer from './FinalBoardContainer';


import { Games } from '../../../../shared/types/Games';
const game: Games = {
    
}


describe('FinalBoard', () => {

    it('FinalBoard Render', () => {
        const tree = renderer
            .create(<Provider store={store}>
                <FinalBoardContainer game={game} />
            </Provider>)
            .toJSON();

        expect(tree).toMatchSnapshot();
    });

});