import { Game } from './types'

export default function openCells(game: Game, row: number, col: number) {
    /** Desde la posicion donde hicimos click recorremos el Array (Eje Y ) -Y <= Y de arriba hacia abajo**/
    for (let i = -3; i <= 3; i++) {

        /*** Recorremos la segunda matriz (Eje X) desde la posicion donde hicimos clik -X <= X de derecha a izquierda***/
        for (let j = -5; j <= 5; j++) {
            try {
                let cell = game.mines[row + i][col + j];
                if (cell != undefined && !cell.open && cell.flag == 'none' && !cell.mine && cell.nearMines <= 1) {
                    /** Abrimos las celdas que entren en el if**/
                    game.mines[row + i][col + j].open = true
                    /** Sumamos uno a la cantidad de casilleros abiertos **/
                    game.opened++
                }


            } catch (error) {
                break;
            }
        }
    }
}

