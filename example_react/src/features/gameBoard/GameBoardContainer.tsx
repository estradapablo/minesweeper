import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Games } from '../../shared/types/Games';

import { getGame, nextTurn, actions } from '../../model/actions/game';
import GameBoardPresentational from './GameBoardPresentational';

type Props = {
	game: Games,
	getGame: Function,
	nextTurn: Function,
	actions: Function,
	match?: any,
	loading: boolean,
	loadingTurn: boolean,
	error: boolean
};

class GameBoardContainer extends React.Component<Props> {

	componentDidMount = () => {
		const { id } = this.props.match.params;
		this.props.getGame({ id: id });
	};

	handleNextTurn = (row: string, col: string, id: string, open: boolean) => {
		const { statusGame } = this.props.game
		if(statusGame !== 'in_process' || open){return false;}
		this.props.nextTurn({ row, col, id: id })
	}

	handleActions = (e:any, row: string, col: string, id: string, open: boolean) => {
		
		const { statusGame } = this.props.game
		const { loadingTurn } = this.props
		if(statusGame !== 'in_process' || loadingTurn){return false;}
		
		if(this.props.game?.statusGame === 'game_over' || this.props.game?.statusGame === 'finished' || open){return false;}
		e.preventDefault();
		this.props.actions({ row, col, id: id })

	}


	render() {

		const { game, loading, error } = this.props
		return (
			<GameBoardPresentational loading={loading} error={error} game={game} handleNextTurn={this.handleNextTurn} handleActions={this.handleActions}/>
		);
	}
}


const mapStateToProps = (state: any) => ({
	game: state.game.game,
	loading: state.game.isFetching,
	loadingTurn: state.game.isFetchingTurn,
	error: state.game.didInvalidate
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
	getGame,
	nextTurn,
	actions
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(GameBoardContainer)