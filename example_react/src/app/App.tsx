import React from 'react';
import ReduxHOC from './redux';
import Routes from './Routes';
import { initializeAxios } from "./axios";

class App extends React.Component {

  componentWillMount() {
    initializeAxios();

  }

  render() {
    return (
      <Routes />
    );
  }
}

const reduxApp = ReduxHOC(App);
export default reduxApp;