import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../model/Store';

import GameBoardPresentational from './GameBoardPresentational';


export default {
    title: 'GameBoard',
    component: GameBoardPresentational,
};

const game = {
    "id": "id",
    "status": "in_process",
    "gameTurn": 1,
    "numberMines": 25,
    "opened": 1,
    "cols": 10,
    "rows": 10,
    "mines": [
        [
            {
                "col": 0,
                "row": 0,
                "mine": false,
                "open": true,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 1,
                "row": 0,
                "mine": false,
                "open": true,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 2,
                "row": 0,
                "mine": false,
                "open": true,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 3,
                "row": 0,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 4,
                "row": 0,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 5,
                "row": 0,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 6,
                "row": 0,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 7,
                "row": 0,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 8,
                "row": 0,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 9,
                "row": 0,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 1,
                "mine": false,
                "open": true,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 1,
                "row": 1,
                "mine": false,
                "open": true,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 2,
                "row": 1,
                "mine": false,
                "open": true,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 3,
                "row": 1,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 4,
                "row": 1,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 5,
                "row": 1,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 6,
                "row": 1,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 7,
                "row": 1,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 8,
                "row": 1,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 9,
                "row": 1,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 2,
                "mine": false,
                "open": true,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 1,
                "row": 2,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 2,
                "row": 2,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 3,
                "row": 2,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 4,
                "row": 2,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 5,
                "row": 2,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 6,
                "row": 2,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 7,
                "row": 2,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 8,
                "row": 2,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 9,
                "row": 2,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 3,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 1,
                "row": 3,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 2,
                "row": 3,
                "mine": false,
                "open": false,
                "nearMines": 4,
                "flag": false
            },
            {
                "col": 3,
                "row": 3,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 4,
                "row": 3,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 5,
                "row": 3,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 6,
                "row": 3,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 7,
                "row": 3,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 8,
                "row": 3,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 9,
                "row": 3,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 4,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 1,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 4,
                "flag": false
            },
            {
                "col": 2,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 3,
                "row": 4,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 4,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 5,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 6,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 7,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 8,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 9,
                "row": 4,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 1,
                "row": 5,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 2,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 3,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 4,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 5,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 6,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 7,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 8,
                "row": 5,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 9,
                "row": 5,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 6,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 1,
                "row": 6,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 2,
                "row": 6,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 3,
                "row": 6,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 4,
                "row": 6,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 5,
                "row": 6,
                "mine": false,
                "open": false,
                "nearMines": 4,
                "flag": false
            },
            {
                "col": 6,
                "row": 6,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 7,
                "row": 6,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 8,
                "row": 6,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 9,
                "row": 6,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 7,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 1,
                "row": 7,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 2,
                "row": 7,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 3,
                "row": 7,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 4,
                "row": 7,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 5,
                "row": 7,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 6,
                "row": 7,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 7,
                "row": 7,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 8,
                "row": 7,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 9,
                "row": 7,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 8,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 1,
                "row": 8,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 2,
                "row": 8,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 3,
                "row": 8,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 4,
                "row": 8,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 5,
                "row": 8,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 6,
                "row": 8,
                "mine": false,
                "open": false,
                "nearMines": 3,
                "flag": false
            },
            {
                "col": 7,
                "row": 8,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 8,
                "row": 8,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 9,
                "row": 8,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            }
        ],
        [
            {
                "col": 0,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 1,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 2,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 3,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 4,
                "row": 9,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 5,
                "row": 9,
                "mine": true,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 6,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 2,
                "flag": false
            },
            {
                "col": 7,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 0,
                "flag": false
            },
            {
                "col": 8,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            },
            {
                "col": 9,
                "row": 9,
                "mine": false,
                "open": false,
                "nearMines": 1,
                "flag": false
            }
        ]
    ]
}

export const GameBoard = () => <Provider store={store}>
    <GameBoardPresentational loading={false} error={false} game={game} />
</Provider>;

GameBoard.story = {
    name: 'General',
};


