import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Login from '../features/login/LoginContainer';
import GameBoard from '../features/gameBoard/GameBoardContainer';

function AppRouter() {
    return (
        <Router>
            <Route path="/" exact component={Login} />
            <Route path="/game-board/:id" exact component={GameBoard} />
        </Router>
    );
}

export default AppRouter;
