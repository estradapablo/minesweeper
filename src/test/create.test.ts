import * as AWSMock from "aws-sdk-mock";
import * as AWS from "aws-sdk";
import { Put } from "aws-sdk/clients/dynamodb";
import { expect } from 'chai';


import { handlerRequests } from '../index'
const LambdaTester = require('lambda-tester');

describe("Minesweeper API TEST /$env/create", () => {

    AWSMock.setSDKInstance(AWS);
    AWSMock.mock('DynamoDB.DocumentClient', 'put', (params: Put, callback: Function) => {
        callback(null, null);
    })

    it("Create a New Game in DynamoDB - Successfully", async () => {

        const params: any = {
            "cols": 10,
            "rows": 10,
            "numberMines": 25,
            "username": "Pablo"
        };

        await LambdaTester(handlerRequests)
            .event({
                httpMethod: 'POST',
                path: '/create',
                body: JSON.stringify(params)
            })
            .expectResult((result) => {

                result.body = JSON.parse(result.body)
                expect(result).to.have.property('statusCode');
                expect(result.statusCode).to.equal(200);
                expect(result).to.have.property('body');
                expect(result.body).to.have.property('id');

            });

    });


    it("Create a New Game in DynamoDB - Error", async () => {

        const params: any = {
            "cols": 10,
            "rows": 10,
            "numberMines": 25,
            /*"username": "Pablo"*/
        };

        await LambdaTester(handlerRequests)
            .event({
                httpMethod: 'POST',
                path: '/create',
                body: JSON.stringify(params)
            })
            .expectResult((result) => {

                result.body = JSON.parse(result.body)
                expect(result).to.have.property('statusCode');
                expect(result.statusCode).to.equal(422);

            });

    });

});