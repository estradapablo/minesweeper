# Bons Game

Welcome to Minesweeper Game

## Built With

* [NodeJs](https://nodejs.org/es/)
* [Type Script](https://www.typescriptlang.org/docs/home.html)
* [ApiGateway](https://docs.aws.amazon.com/es_es/apigateway) - For Api Implementation
* [DynamoDB](https://aws.amazon.com/es/dynamodb/) - For database
* [@hapi/joi](https://www.npmjs.com/package/@hapi/joi) - For validation request API
* [Mocha](https://mochajs.org/) - For Unit Test
* [serverless-offline](https://www.npmjs.com/package/serverless-offline) - For emulates AWS λ and API Gateway on your local machine
* [serverless-dynamodb-local](https://www.npmjs.com/package/serverless-dynamodb-local) - For local environment
* [lambda-tester](https://www.npmjs.com/package/lambda-tester) - For unit tests for AWS Lambda.
* [apiDoc](https://apidocjs.com/) - Inline Documentation for RESTful web APIs
* [S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html) - For deploy online react example

# Online Demo:

http://buscaminas.s3-website-us-east-1.amazonaws.com/


## Dev Environment


### Installing dependencies

```
yarn
```
or
```
npm install
```

### Set .env for mock Dynamo DB.

IS_OFFLINE=true

### For use serverless-dynamodb-local

Java Runtime Engine (JRE) version 6.x or newer

Start DynamoDB with:
```
yarn start:db
```

### For use serverless-offline

```
yarn start:dev
```

### Running the tests

```
yarn test
```

### End to end tests

```
$ NODE_ENV=test ts-mocha 'src/test/*.test.ts' --exit --bail --timeout 50000

  Minesweeper API TEST /$env/action
    ✓ Action - Successfully (63ms)
    ✓ Action- Error

  Minesweeper API TEST /$env/create
    ✓ Create a New Game in DynamoDB - Successfully
    ✓ Create a New Game in DynamoDB - Error

  Minesweeper API TEST /$env/get-game
    ✓ Get Game in DynamoDB - Successfully
    ✓ Get Game in DynamoDB - Error

  Minesweeper API TEST /$env/next-turn
    ✓ Next Turn - Successfully
    ✓ Next Turn - Error

  8 passing (88ms)

Done in 3.50s.

```

## PROD Environment

### Installing the AWS CLI

(https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)

### Set credentials in aws

```
aws configure
AWS Access Key ID [None]: 
AWS Secret Access Key [None]:
```
For more information (https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)

### Comand Deployment

```
yarn deploy
```

## Remove last deploy

```
yarn remove
```
## API Doc

ApiDoc is found in (/apidoc)

For generate new api doc:

```
yarn apidoc
```

## POSTMAN Colecction 

https://www.getpostman.com/collections/e132d3973131e65e45c2


## Example in react

### For Testing mode 
```
cd example_react

yarn

/** Set in .env REACT_APP_API_URL for localhost: http://localhost:3000 for Aws api gateway check deploy response **/

yarn start

```

### For Production mode 

```
cd example_react

yarn

/** Set in .env REACT_APP_API_URL check deploy response for Aws api gateway URL's**/

yarn build


/** Deploy in S3 Bucket **/
yarn deploy

```



## Authors

* **Pablo Estrada** - *Initial work* - [Pablo Estrada](https://bitbucket.org/estradapablo/)

