import React from 'react';
import { Container, Row, Col, Input, Button } from 'reactstrap';

import '../../shared/styles/login.css';

type Props = {
  handleOnChangeName: Function,
  handleCreate: Function,
  handleOnChangeRows: Function,
  handleOnChangeCols: Function,
  handleOnChangeMines: Function,
  username: string,
  rows: string,
  cols: string,
  numberMines: string
};

function LoginPresentational(props: Props) {

  const { handleOnChangeName, 
          handleOnChangeRows, 
          handleOnChangeCols, 
          handleOnChangeMines, 
          username,
          rows,
          cols,
          numberMines, 
          handleCreate } = props

  return (
    <Container className="login d-flex h-100 justify-content-center">
      <Row className="align-self-center">
        <Col>
          <h1>Welcome to Mine Sweeper</h1>
          <h5 className="mt-4">What's your name?</h5>
          <Input id="login-input-name" name="login-input-name" value={username} placeholder="NAME" className="mt-0 login-input-color b-none" onChange={(e) => handleOnChangeName(e.target.value)} />
          <h5 className="mt-4">Default Rows</h5>
          <Input id="login-input-rows" name="login-input-rows" value={rows} placeholder="ROWS" className="mt-0 login-input-color b-none" onChange={(e) => handleOnChangeRows(e.target.value)} />
          <h5 className="mt-4">Default Cols</h5>
          <Input id="login-input-cols" name="login-input-cols" value={cols} placeholder="COLS" className="mt-0 login-input-color b-none" onChange={(e) => handleOnChangeCols(e.target.value)} />
          <h5 className="mt-4">Default numberMines</h5>
          <Input id="login-input-numberMines" name="login-input-numberMines" value={numberMines} placeholder="MINES" className="mt-0 login-input-color b-none" onChange={(e) => handleOnChangeMines(e.target.value)} />

          <Button
            disabled={!username ? true : false}
            onClick={()=>{handleCreate()}}
            color="primary"
            className="mt-4 w-100 font-weight-bold" >
            Let's Play
          </Button>
        </Col>
      </Row>
    </Container>
  );
}

export default LoginPresentational