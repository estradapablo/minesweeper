import * as AWSMock from "aws-sdk-mock";
import * as AWS from "aws-sdk";
import { Update, Get } from "aws-sdk/clients/dynamodb";
import { expect } from 'chai';

const getQueryResponse = require("./mockData/get_response_next_turn.json")
const updateQueryResponse = require("./mockData/update_query_response.json")

const nextTurnResponse = require("./mockData/next_turn_response.json")


import { handlerRequests } from '../index'
const LambdaTester = require('lambda-tester');

describe("Minesweeper API TEST /$env/next-turn", () => {

    AWSMock.setSDKInstance(AWS);
    AWSMock.mock('DynamoDB.DocumentClient', 'get', (query: Get, callback: Function) => {
        callback(null, getQueryResponse);
    })

    AWSMock.mock('DynamoDB.DocumentClient', 'update', (params: Update, callback: Function) => {
        callback(null, updateQueryResponse);
    })

    it("Next Turn - Successfully", async () => {

        const params: any = {
            "id": "0acae710-2c41-43ea-8c17-b969e3e82baf",
            "row": 1,
            "col": 1
        };

        await LambdaTester(handlerRequests)
            .event({
                httpMethod: 'POST',
                path: '/next-turn',
                body: JSON.stringify(params)
            })
            .expectResult((result) => {

                result.body = JSON.parse(result.body)

                expect(result).to.have.property('statusCode');
                expect(result.statusCode).to.equal(200);
                expect(result.body.id).to.equal(nextTurnResponse.id);
                expect(result.body.mines.length).to.equal(10);
                expect(result.body.mines[1][1].open).to.equal(true);
           
            });

    });

    it("Next Turn - Error", async () => {

        const params: any = {
        };

        await LambdaTester(handlerRequests)
            .event({
                httpMethod: 'POST',
                path: '/next-turn',
                body: JSON.stringify(params)
            })
            .expectResult((result) => {

                result.body = JSON.parse(result.body)

                expect(result).to.have.property('statusCode');
                expect(result.statusCode).to.equal(422);

            });

    });


});