import { schema } from '../models/actionsNextTurn'
import { ParamsActionsNextTurn } from '../helpers/types';
import queryGetGames from '../querys/queryGetGames'
import queryUpdateGames from '../querys/queryUpdateGame'

import setMines from '../helpers/setMines'
import nearMines from '../helpers/nearMines'
import openCells from '../helpers/openCells'

/**
 * @api {post} /$env/next-turn Pass Next Turn
 * @apiVersion 1.0.0
 * @apiName NextTurn
 * @apiGroup Game
 *
 * @apiParam {String} id id of the game.
 * @apiParam {Number} row Cliked position of Row.
 * @apiParam {Number} col Cliked position of Col.
 *
 * @apiParamExample {json} Request-Example:
 *   {
 *   "id": "027936e9-1c24-4875-bbc3-5f12e3d73a32",
 *   "row": 8,
 *   "col": 7
 *   }
 *
 * @apiSuccess {Array} mines Mines field.
 * @apiSuccess {Boolean} mines.mine If it's a Mine
 * @apiSuccess {Number} mines.col Col Position
 * @apiSuccess {Number} mines.nearMines Near Mines from this position
 * @apiSuccess {Number} mines.row Row position
 * @apiSuccess {String} mines.flag If it's a flag true ('none', 'flag', 'question')
 * @apiSuccess {Boolean} mines.open If it's Open
 * 
 * @apiSuccess {Number} gameTurn Current Turn of the game.
 * @apiSuccess {String} statusGame Current status of the game ('in_process', 'finished', 'game_over').
 * @apiSuccess {Number} opened Numer of fields opened.
 * @apiSuccess {String} id id of the game.
 * @apiSuccess {Number} rows Number of rows in the game.
 * @apiSuccess {Number} initNumberMines Initial number of mines in the game.
 * @apiSuccess {String} username Username of the game.
 * @apiSuccess {Number} numberMines Current number of mines flaged.
 * 
 *  * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *  {
 *      "mines": [
 *          [
 *              {
 *                  "mine": true,
 *                  "col": 0,
 *                  "nearMines": 0,
 *                  "row": 0,
 *                  "flag": "none",
 *                  "open": false
 *              }
 *          ]
 *      ],
 *      "gameTurn": 1,
 *      "statusGame": "in_process",
 *      "opened": 1,
 *      "id": "9bbeb618-0677-4f29-a4d4-1c5f6aa77ff3",
 *      "rows": 1,
 *      "cols": 1,
 *      "initNumberMines": 25,
 *      "username": "Pablo",
 *      "numberMines": 25
 *  }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 442 Unprocessable Entity
 * {
 *    "statusCode": 422,
 *    "message": {
 *        "message": "No Game with this id: 10129f68-768d-4b96-8810-fe6e6825c3fe"
 *    }
 * }
 * 
 * @apiErrorExample {json} Error-Validation-Response:
 *     HTTP/1.1 442 Unprocessable Entity
 *   {
 *       "_original": {},
 *       "details": [
 *           {
 *               "message": "\"id\" is required",
 *               "path": [
 *                   "id"
 *               ],
 *               "type": "any.required",
 *               "context": {
 *                   "label": "id",
 *                   "key": "id"
 *               }
 *           }
 *       ],
 *       "statusCode": 422
 *   }
 */

const NextTurn = async (params: ParamsActionsNextTurn) => {

  try {
    
    /** Validamos los parametros segun el esquema definido en Models**/
    await schema.validateAsync(params);

    let { id, row, col } = params;
    /** Funcion para buscar el juego en la base **/
    let game = await queryGetGames({ id })
    let { cols, rows, initNumberMines, numberMines, mines, gameTurn, statusGame } = game;
    
    /** Si el turno es 0 creamos las minas, para que nunca el primer clik sea una mina **/
    if (gameTurn == 0) {
      /** Funcion para setear minas **/
      setMines(numberMines, mines, rows, cols, row, col);
      // Recorremos todas las casillas de alrededor y vemos si son minas para colocar el numero
      nearMines(game)

    } else if (statusGame === 'game_over') {
      /** Si ya termino el juego **/
      return game
    } else if(!mines[row][col]){
      /** Si no existe item en esta posicion devolvemos error**/
      let err = {
        statusCode: 422,
        message: 'No Mine in this combination row:' + row + ' col:'+ col
      };
      return err
    }else if (mines[row][col].open === true) {
      /** Si ya esta abierta esta posicion, no hacemos nada **/
      return game
    }

    /** incrementamos en 1 los turnos **/
    game.gameTurn++

    /** Abrimos la posicion **/
    game.mines[row][col].open = true;

    /** Sumamos uno a la cantidad de casilleros abiertos **/
    game.opened++

    /** Si el campo tiene una bomba **/
    if (mines[row][col].mine) {
      game.statusGame = 'game_over';
    } else {
      /** Si no perdio ni termino, hacemos en forma recursiva el recorrido para desbloquear las cercanas que no tienen minas **/
      openCells(game, row, col)

      if ((game.cols * game.rows) - initNumberMines == game.opened) {
        /** Si la candidad de casilleros abiertos es igual a la cantidad de casilleros menos las minas, ganamos **/
        game.statusGame = 'finished';
      }

    }
    
    /** Guardamos el juego antes de mandar la respuesta **/
    return queryUpdateGames(game)
  }
  catch (err) {
    /** Si no paso la validacion del request, devolvemos error **/
    err.statusCode = 422;
    return err
  }

}

export default NextTurn


