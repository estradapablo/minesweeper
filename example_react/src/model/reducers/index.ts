import { combineReducers } from 'redux';
import login from './login';
import game from './game';

const allReducers = combineReducers({
  login,
  game
});

const rootReducer = (state: any, action: any) => {
  return allReducers(state, action);
};

export default rootReducer;