import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { render } from '@testing-library/react';

import { Provider } from 'react-redux';
import { store } from '../../model/Store';

import GameBoardContainer from './GameBoardContainer';

const params = {
    params: {
        id: '60b55dba-f58c-404e-96f9-6ee1cb6126b6'
    }
}

describe('Game Board', () => {


    it('GameBoard Render', async () => {
        const { container } = render(
            <MemoryRouter>
                <Provider store={store}>
                    <GameBoardContainer match={params} />
                </Provider>
            </MemoryRouter>
        );

        expect(container).toMatchSnapshot();
    });

});



