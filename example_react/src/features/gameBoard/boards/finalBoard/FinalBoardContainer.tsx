import React from 'react';

import { Container, Row, Col, Button } from 'reactstrap';
import { Games } from '../../../../shared/types/Games';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createGame } from '../../../../model/actions/game';


import { Icon } from 'react-icons-kit'
import { ic_sentiment_dissatisfied } from 'react-icons-kit/md/ic_sentiment_dissatisfied'
import { ic_sentiment_very_satisfied } from 'react-icons-kit/md/ic_sentiment_very_satisfied'

type Props = {
    game: Games,
    handleOnCreate: Function,
    login: object
};


function FinalBoardContainer(props: Props) {

    const { game, handleOnCreate, login } = props

    const handleCreate = () => {

        handleOnCreate(login).then((response: any) => {

            if (response.value.game.id) {
                
                window.location.assign("/game-board/" + response.value.game.id);
            } else {
                alert('Error to create a new game!')
            }

        })

    }

    return (
        <Container className={ (game.statusGame === 'finished' || game.statusGame === 'game_over' ? '' : 'd-none') + " board-background-dark final-board"}>
        <Row className="font-weight-bold text-center">
            <Col>
                {
                    game.statusGame === 'finished' ?

                        <React.Fragment>
                            <div className="w-100"><Icon icon={ic_sentiment_very_satisfied} size={54} /></div>
                            <div className="w-100 mt-4">Congratulations!</div>
                            <div className="w-100">You Win!</div>
                        </React.Fragment>

                        : null
                }

                {
                    game.statusGame === 'game_over' ?
                        <React.Fragment>
                            <div className="w-100"><Icon icon={ic_sentiment_dissatisfied} size={54} /></div>
                            <div className="w-100 mt-4">Ups.. you lose!</div>
                        </React.Fragment>
                        : null
                }

            </Col>
        </Row>

        <Row>

            <Col lg={12} xl={12}>
                <Button
                    onClick={() => { handleCreate() }}
                    color="primary"
                    className="mt-4 w-100 font-weight-bold" >
                    Let's Play
                </Button>
            </Col>
        </Row>

    </Container>


    );

}

const mapStateToProps = (state: any) => ({
	login: state.login
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    handleOnCreate: createGame
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FinalBoardContainer)