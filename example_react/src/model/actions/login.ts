export const CHANGE_NAME = 'CHANGE_NAME';
export const CHANGE_ROWS = 'CHANGE_ROWS';
export const CHANGE_COLS = 'CHANGE_COLS';
export const CHANGE_MINES = 'CHANGE_MINES';

type Action = { type: string, payload: string, };

export const changeName = (payload: string): Action => {
    return { type: CHANGE_NAME, payload: payload };
};

export const changeRows = (payload: string): Action => {
    return { type: CHANGE_ROWS, payload: payload };
};

export const changeCols = (payload: string): Action => {
    return { type: CHANGE_COLS, payload: payload };
};

export const changeMines = (payload: string): Action => {
    return { type: CHANGE_MINES, payload: payload };
};



