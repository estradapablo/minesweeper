import { schema } from '../models/getGame'
import { ParamsGet } from '../helpers/types';
import queryGetGames from '../querys/queryGetGames'

/**
 * @api {post} /$env/get-game Get a Game
 * @apiVersion 1.0.0
 * @apiName GetName
 * @apiGroup Game
 *
 * @apiParam {String} id id to get game.
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *      "id": "10129f68-768d-4b96-8810-fe6e6825c3fe"
 *     }
 *
 * @apiSuccess {Array} mines Mines field.
 * @apiSuccess {Boolean} mines.mine If it's a Mine
 * @apiSuccess {Number} mines.col Col Position
 * @apiSuccess {Number} mines.nearMines Near Mines from this position
 * @apiSuccess {Number} mines.row Row position
 * @apiSuccess {String} mines.flag If it's a flag true ('none', 'flag', 'question')
 * @apiSuccess {Boolean} mines.open If it's Open
 * 
 * @apiSuccess {Number} gameTurn Current Turn of the game.
 * @apiSuccess {String} statusGame Current status of the game ('in_process', 'finished', 'game_over').
 * @apiSuccess {Number} opened Numer of fields opened.
 * @apiSuccess {String} id id of the game.
 * @apiSuccess {Number} rows Number of rows in the game.
 * @apiSuccess {Number} initNumberMines Initial number of mines in the game.
 * @apiSuccess {String} username Username of the game.
 * @apiSuccess {Number} numberMines Current number of mines flaged.
 * 
 *  * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *  {
 *      "mines": [
 *          [
 *              {
 *                  "mine": false,
 *                  "col": 0,
 *                  "nearMines": 0,
 *                  "row": 0,
 *                  "flag": "none",
 *                  "open": false
 *              }
 *          ]
 *      ],
 *      "gameTurn": 0,
 *      "statusGame": "in_process",
 *      "opened": 0,
 *      "id": "9bbeb618-0677-4f29-a4d4-1c5f6aa77ff3",
 *      "rows": 1,
 *      "cols": 1,
 *      "initNumberMines": 25,
 *      "username": "Pablo",
 *      "numberMines": 25
 *  }
 * 
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 442 Unprocessable Entity
 * {
 *    "statusCode": 422,
 *    "message": {
 *        "message": "No Game with this id: 10129f68-768d-4b96-8810-fe6e6825c3fe"
 *    }
 * }
 * 
 * @apiErrorExample {json} Error-Validation-Response:
 *     HTTP/1.1 442 Unprocessable Entity
 *   {
 *       "_original": {},
 *       "details": [
 *           {
 *               "message": "\"id\" is required",
 *               "path": [
 *                   "id"
 *               ],
 *               "type": "any.required",
 *               "context": {
 *                   "label": "id",
 *                   "key": "id"
 *               }
 *           }
 *       ],
 *       "statusCode": 422
 *   }
 */

const GetGame = async (params: ParamsGet) => {

    try {
        /** Validamos los parametros segun el esquema definido en Models**/
        await schema.validateAsync(params);

        /** Buscamos el juego para ese id **/
        return queryGetGames(params)
    }
    catch (err) {
        /** Si no paso la validacion del request, devolvemos error **/
        err.statusCode = 422;
        return err
    }



}

export default GetGame